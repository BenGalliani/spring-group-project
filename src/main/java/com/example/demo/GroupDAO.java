package com.example.demo;

import com.example.demo.Group;

import javax.sql.DataSource;

public interface GroupDAO
{
    public void Insert(Group group);
    public Group findById(int groupID);
}
