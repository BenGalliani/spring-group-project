package com.example.demo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

public class jdbcGroupDAO implements GroupDAO {
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void Insert(Group group){

        String sql = "INSERT INTO T6_Credentials" +
                "(oid, groupid, account, password, salt,create_user,create_timestamp) VALUES (NULL , ?, ?, ?, ?, ?, ?)";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, group.getGroupId());
            ps.setString(2, group.getAccount());
            ps.setString(3, group.getPassword());
            ps.setString(4, group.getSalt());
            ps.setString(5, group.getCreate_user());
            ps.setTimestamp(6, group.getCreate_timestamp());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }

    public Group findById(int Id){

        String sql = "SELECT * FROM T6_Credentials WHERE ID = ?";

        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, Id);
            Group group = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                group = new Group(
                        rs.getInt("ID"),
                        rs.getString("groupId"),
                        rs.getString("account"),
                        rs.getString("password"),
                        rs.getString("salt"),
                        rs.getString("create_user"),
                        rs.getTimestamp("create_timestamp")
                );
            }
            rs.close();
            ps.close();
            return group;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }
}
