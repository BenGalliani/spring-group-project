package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name="Credentials")
public class Group {

    int ID;
    String groupId;
    String account;
    String password;
    String salt;
    String create_user;
    Timestamp create_timestamp;


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    public Timestamp getCreate_timestamp() {
        return create_timestamp;
    }

    public void setCreate_timestamp(Timestamp create_timestamp) {
        this.create_timestamp = create_timestamp;
    }

    Group(int id, String groupId, String acc, String password, String salt, String user, Timestamp timestamp) {
        this.ID = id;
        this.account = acc;
        this.groupId = groupId;
        this.password = password;
        this.salt = salt;
        this.create_user = user;
        this.create_timestamp = timestamp;
    }
}

