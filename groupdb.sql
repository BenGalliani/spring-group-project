-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2018 at 09:11 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `groupdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `credentials`
--

CREATE TABLE `credentials` (
  `oid` mediumint(9) NOT NULL,
  `groupid` varchar(256) NOT NULL,
  `account` varchar(256) NOT NULL,
  `password` varchar(512) NOT NULL,
  `salt` varchar(256) NOT NULL,
  `create_user` varchar(256) NOT NULL,
  `create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentials`
--

INSERT INTO T6_Credentials (`id`, `groupName`, `cName`, `cPass`, `create_timestamp`) VALUES
(1, 'group3', 'acc1', '$2y$10$otGehf/sfTYqWd1Smxllre0gsXJBmt7fvs6Js/Vz64XIo4SFDZoGm', '2018-01-27 04:28:31'),
(2, 'group3', 'acc2', '$2y$10$Fbf0sv7Mwg.I6OQVtAzUTemiyQA5haqBV0pidwvcLZzPuKLSZvYvm', '2018-01-27 04:33:07'),
(3, '2ndG', 'new', '$2y$10$u0JOYxgHPMkADOzHfrMQA.czM/DhCg3wed2aRftFvwdAJJG5ILijO', '2018-01-27 15:37:06'),
(4, 'group9999', 'acc3', '$2y$10$.hNbxChl9IkoeOHJj2P4jeVlLnMcljTmPFdKIRtFROb3t9BiUtqKC', '2018-01-29 20:12:01'),
(5, 'group9999', 'acc4', '$2y$10$19vpTxyig9H249H18vGrkunI9iljL9XJz3Lhs9/oPPCb0Tnq36vTi', '2018-01-29 20:12:26'),
(6, 'g1', 'a1', '$2y$10$NNHgF2efxdkKe4..FaOEeOdgmXgE91XMXdTc.uXsZ6GrZAhoPyAda', '2018-01-29 20:41:56'),
(7, 'g1', 'a2', '$2y$10$87gezQC0OdLJuwqLK9czcewXL6YVDsUOA1RBF82v6xbXAveddi74u', '2018-01-29 20:42:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `credentials`
--
ALTER TABLE T6_Credentials
  ADD PRIMARY KEY (`oid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `credentials`
--
ALTER TABLE T6_Credentials
  MODIFY `oid` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
